<?php
$errors = array();

session_start();

   require("include/config.php");
   require("include/db.php");

   //preventing sql injections
   function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
    }

   if (isset($_POST['login'])) {
   $user = test_input($_POST['user']);
   $user = mysqli_real_escape_string($connection,$user);
   $password = test_input($_POST['password']);
   $password = mysqli_real_escape_string($connection,$password);

   $password_temp = SALT1."$password".SALT2;

    $sql = "SELECT id_user,concat(firstname) as name FROM users
            WHERE username = '$user'
            AND password = MD5('$password_temp')"; 

    $result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

    if (mysqli_num_rows($result)>0) {
    	while ($record = mysqli_fetch_array($result,MYSQLI_BOTH)) {
     	    $_SESSION['id_user'] = $record['id_user'];
            $_SESSION['name'] = $record['name'];
            header("Location:index.php");
        }
    } else {
        array_push($errors, "Pogrešno korisničko ime/lozinka");
      }
   }
?>