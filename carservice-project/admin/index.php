<?php 
require("login-check.php");
?>
<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<!-- Apple devices fullscreen -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- Apple devices fullscreen -->
	<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />

	<title>Automehaničarska radnja | Admin</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- jQuery UI -->
	<link rel="stylesheet" href="css/plugins/jquery-ui/jquery-ui.min.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="css/style.css">
	<!-- Color CSS -->
	<link rel="stylesheet" href="css/themes.css">
	<!--font awesome-->
	<link rel="stylesheet" href="fonts/font-awesome.min.css">

	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>

	<!-- jQuery UI -->
	<script src="js/plugins/jquery-ui/jquery-ui.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>

	<!-- Theme scripts -->
	<script src="js/application.min.js"></script>


</head>

<body data-layout-topbar="fixed">
	
	
	<?php include ('header.html'); ?>
	
	<div class="container-fluid" id="content">
		
		 <?php 
 
        
        if(!isset($_GET["link"])) 
        $_GET["link"]="index"; 
        
        switch ($_GET["link"]) {   
            
			
			case "pocetna":  
            include("pocetna.php");  
            break;  
			
			case "korisnici":  
            include("korisnici.php");  
            break;  
            
            case "status":  
            include("status.php");  
            break;  
            
			case "usluge":  
            include("usluge.php");  
            break;  
			
            case "prijave":  
            include("prijave.php");  
            break; 
			
			case "dodaj-novu-uslugu":  
            include("service-insert.php");  
            break;
			
			case "nova-unesena-usluga":  
            include("realServicesInsert.php");  
            break;
			
			case "izmeni-postojecu-uslugu":  
            include("service-update.php");  
            break;
			
			case "izmenjena-usluga":  
            include("realServicesUpdate.php");  
            break;
			
			case "izmeni-prijavu-servisa":  
            include("problems-update.php");  
            break;
			
			case "izmenjena-prijava-servisa":  
            include("realProblemsUpdate.php");  
            break;
			
            default:  
            include("pocetna.php");  
            break; 

            } 
?>
	</div>
</body>

</html>
