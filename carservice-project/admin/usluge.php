<?php include ('sidebar.html'); ?>
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h1>Usluge</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="fa fa-table"></i>
									Naziv, opis i cena usluge
								</h3>
							</div>
							<div class="box-content">	
									<a href="index.php?link=dodaj-novu-uslugu" class="btn btn-red">Dodaj novu uslugu</a>						
							</div>
							<div class="box-content nopadding">							
	<?php
define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
require('db_config.php');

$sql = "SELECT services.service_id, services.service_type, services.service_description, services.price FROM services";
$result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

if(mysqli_num_rows($result)>0)
{
echo "<table class=\"table table-hover table-nomargin table-bordered \">
	 <thead>
            <tr>
                <th>Id usluge</th>
                <th>Naziv usluge</th>
                <th>Opis usluge</th>
                <th>Cena usluge</th>
				<th>Opcije</th>
			</tr>
		</thead>	
			
			";


    while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)) // MYSQLI_ASSOC, MYSQLI_BOTH,MYSQLI_NUM
    {
        
		$rowID = $row["service_id"];
		echo "<tbody>";
        echo "<tr>";
        echo "<td>" . $row['service_id'] . "</td>";  
        echo "<td>" . $row['service_type'] . "</td>";
        echo "<td>" . $row['service_description'] . "</td>";
        echo "<td>" . $row['price'] . "</td>";
		echo "<td> <a href='service-update.php?service_id=$rowID' class=\"btn\" rel=\"tooltip\" title=\"Izmeni\"><i class=\"fa fa-edit\"></i></a>
		<a href='delete-services.php?service_id=$rowID' class=\"btn\" rel=\"tooltip\" title=\"Izbriši\" onclick=\"myFunction()\"><i class=\"fa fa-times\"></i></a></td>";
		
        echo "</tr>";
		echo "</tbody>";

    }
	echo "</table>";
    mysqli_free_result($result);
}

mysqli_close($connection);
?>																											
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<script>
			function myFunction() {
    			alert("Uspešno ste izbrisali uslugu!");
			}
		</script>