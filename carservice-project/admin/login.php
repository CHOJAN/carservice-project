<?php
$error_msg = "";
if(isset($_POST["username"])&&isset($_POST["upw"])) {

	$user = $_POST["username"];
	$pass = $_POST["upw"];
	$validated = false;
		
	$passwords = array(
		'admin'=>'admin', 
		'someuser' => 'his?pass'
	);
	
	if(isset($passwords[$user])) if($passwords[$user]==$pass) $validated = true;
	

	if($validated) {
		setcookie("username", $user); 
		setcookie("password", MD5($pass)); 
		
		echo "<meta http-equiv='refresh' content='0;url=index.php'>";

	} else {
		$error_msg = "<div class='alert alert-danger'>Pogrešno korisničko ime i/ili lozinka</div>";
	}
}
?>
<!doctype html>
<html>


<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<!-- Apple devices fullscreen -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- Apple devices fullscreen -->
	<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />

	<title>Automehaničarska radnja | Admin | Prijava</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- jQuery UI -->
	<link rel="stylesheet" href="css/plugins/jquery-ui/jquery-ui.min.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="css/style.css">
	<!-- Color CSS -->
	<link rel="stylesheet" href="css/themes.css">
	<!--font awesome-->
	<link rel="stylesheet" href="fonts/font-awesome.min.css">

	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>

	<!-- jQuery UI -->
	<script src="js/plugins/jquery-ui/jquery-ui.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>

	<!-- Theme scripts -->
	<script src="js/application.min.js"></script>

</head>

<body class='login'>
	<div class="wrapper">
		<div class="logo">
			
				<img src="img/logo-big.png">
		<div>
		<div class="login-body">
			<h2>Prijavi se</h2>
			<?php echo $error_msg; ?>
			<form action="" method='post' class='form-validate' id="test">
				<div class="form-group">
					<div class="email controls">
						<input type="text" name='username' placeholder="Korisničko ime" class='form-control' data-rule-required="true" data-rule-email="true">
					</div>
				</div>
				<div class="form-group">
					<div class="pw controls">
						<input type="password" name="upw" placeholder="Lozinka" class='form-control' data-rule-required="true">
					</div>
				</div>
				<div class="submit">
					<input type="submit" value="Prijavi se" class='btn btn-primary'>
				</div>
			</form>
		</div>
	</div>
</body>

</body>
</html>
