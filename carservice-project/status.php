<!DOCTYPE html>

<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Automehaničarska radnja | Provera statusa vozila</title>
        <!-- Favicon -->
        <link rel="icon" href="assets/img/favicon-icon.png">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<!-- Font-awesome CSS -->
		<link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
		<!-- Flaticon CSS -->
		<link rel="stylesheet" href="assets/fonts/flaticon.css">
		<!-- Datepicker CSS -->
		<link rel="stylesheet" href="assets/css/datepicker.css">
		<!-- Animate CSS -->
		<link rel="stylesheet" href="assets/css/animate.css">
		<!-- Style CSS -->
		<link rel="stylesheet" href="assets/css/style.css">
		<!-- Responsive CSS -->
		<link rel="stylesheet" href="assets/css/responsive.css">
		<!-- jQuery min js -->
		<script src="assets/js/jquery-1.12.4.min.js"></script>

</head>

<body>
    <!-- Start Header -->
         <?php include ('header.php'); ?>
        <!-- End Header -->
		
		 <!-- Start Breadcumbs Area -->
        <div class="breadcumbs-area breadcumbs-banner">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h2>Status</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcumbs Area -->
	 <div class="content-block-area">
            <div class="container">	
			<div class="row">
				<div class="col-md-6">
                    <div class="status-boxed-bg status-boxed-image"></div>
                </div>
			
					<div class="col-md-6">
								<div class="section-title text-center">
								   <h2><span>Provera</span> statusa vozila</h2>
								   <div class="icon-cars"><img src="assets/img/cars.png" alt="car"></div>
								   <p> Za proveru statusa vašeg vozila molimo vas da unesete registarski broj auta.</p>
								</div>
			
								<div class="status-form">
								   <div class="row">
										<div class="col-md-12 text-center">
											<label>Registarski broj</label>
											<input type="text" id="plate_number" class="text-center" name="plate_number" value=""  maxlength="7" placeholder="AA000AA">
										</div>
										<div class="row">
											<div class="col-md-12 text-center">
											<button  id="btnOk"  class="btn theme-btn">Proveri status</button>
											</div>
										</div>
									</div>
								</div>
      
							   <div class="status-info text-center">
									<div class="row">
										<h2><span>Trenutni</span> status vašeg vozila</h2>
										<div class="icon-cars"><img src="assets/img/cars.png" alt="car"></div>
											<table class="table">
												<tbody>
													<tr><th>Broj registracije</th><td id="registracija"></td></tr>
													<tr><th>Datum servisa</th><td id="datum"></td></tr>
													<tr><th>Termin</th><td id="vreme"></td></tr>
													<tr><th>Opis problema</th><td id="opis_problema"></td></tr>
													<tr><th>Status vozila</th><td id="status_vozila"></td></tr>
												</tbody>
											</table>
										
									</div>
								</div>
								<div class="status-message text-center"></div>
					</div>
			</div>
		</div>
	</div>
	<!-- End Appointment Area -->
        
        
         <!-- Start Footer -->
         <?php include ('footer.php'); ?>
        <!-- End Footer -->
	
	
    <script type="text/javascript">
        $(document).ready(function() {
            $('#btnOk').click(function() {
                var plate_number = $("#plate_number").val();

                if(plate_number != '') {
                    $.ajax({
                        url: 'get-data.php',
                        type: 'post',
                        dataType: "json",
                        data: {plate_number:plate_number},
                        success: function(data) {
                            if(data.error == '') {
                                $(".status-message").html("");
                                $(".status-info").show();
                                $("#registracija").html(data.car_service.plate_number);                           
                                $("#datum").html(data.car_service.date);
								$("#vreme").html(data.car_service.time);
                                $("#opis_problema").html(data.car_service.problem_description);
								$("#status_vozila").html(data.car_service.status);
                            } else {
                                $(".status-info").hide();
                                $(".status-message").html("Pogrešna/nepostojeća registracija! Pokušajte ponovo.");
                            }
                        }
                    });
                }   
            });
        });
    </script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>

</body>
</html>
