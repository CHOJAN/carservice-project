<?php include('appointment.php'); ?>
<!DOCTYPE html>
<html lang="en">
	
<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Automehaničarska radionica | Zakaži servis</title>
        <!-- Favicon -->
        <link rel="icon" href="assets/img/favicon-icon.png">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<!-- Font-awesome CSS -->
		<link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
		<!-- Flaticon CSS -->
		<link rel="stylesheet" href="assets/fonts/flaticon.css">
		<!-- Datepicker CSS -->
		<link rel="stylesheet" href="assets/css/datepicker.css">
		<!-- Animate CSS -->
		<link rel="stylesheet" href="assets/css/animate.css">
		<!-- Style CSS -->
		<link rel="stylesheet" href="assets/css/style.css">
		<!-- Responsive CSS -->
		<link rel="stylesheet" href="assets/css/responsive.css">
		<!-- jQuery min js -->
		<script src="assets/js/jquery-1.12.4.min.js"></script>
	</head>
	
	<body>
             
        <!-- Start Header -->
         <?php include ('header.php'); ?>
        <!-- End Header -->
        
        <!-- Start Breadcumbs Area -->
        <div class="breadcumbs-area breadcumbs-banner">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h2>Zakaži servis</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcumbs Area -->
        
        
        <!-- Start Appointment Area -->
        <div class="content-block-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="appointment-boxed-bg appointment-boxed-image wow fadeInLeft"></div>
                    </div>
                    <div class="col-md-6">
                       <div class="section-title text-center">
                           <h2><span>Pošalji</span> prijavu</h2>
                           <div class="icon-cars"><img src="assets/img/cars.png" alt="car"></div>
                           <p> Molimo vas da popunite sva tražena polja. Pre slanja upita još jednom proverite vaše unete podatke. Obratite pažnju na registarski broj jer će te sa njim moći proveriti status vašeg vozila.</p>
                       </div>
                        <form class="appointment-form" action="zakazi-servis.php" method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Registarski broj</label>
                                   <input type="text" name="plate_number" placeholder="AA000AA" value="<?php echo $plate_number; ?>">
                                </div>
                                <div class="col-md-6">
                                    <label>Izaberi uslugu</label>
                                    <select type="text" name="select_services"  value="<?php echo $select_services; ?>">
                                        <option value="">-- Izaberi uslugu --</option>
                                        <?php

								  $sql = "SELECT * FROM services ORDER BY service_type ASC";
								  $result = mysqli_query($connection,$sql) or die(mysql_error());

								  if (mysqli_num_rows($result)>0) {
									
									while ($record = mysqli_fetch_array($result,MYSQLI_BOTH)){
									  echo "<option value=\"$record[service_id]\">$record[service_type]</option>";         
									}
								  }
								  ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Rezerviši datum</label>
                                    <input type="text" id="datepicker" name="date" value="" placeholder="Izaberi datum" required>
									<script type='text/javascript'>
$(function() {
		$( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd', maxDate:'+1m +10d', minDate: 0, beforeShowDay: function(date) {
        var day = date.getDay();
        return [(day != 0), ''];
    } });
	});									
</script>
                                </div>
                                <div class="col-md-6">
                                    <label>Rezerviši vreme</label>
                                    <select name="time" value="" id="Time">
                                        <option value="">-- Izaberi vreme --</option>
                                        <option value="09:00-10:00">09:00 - 10:00</option>
                                        <option value="10:00-11:00">10:00 - 11:00</option>
                                        <option value="11:00-12:00">11:00 - 12:00</option>
                                        <option value="12:00-13:00">12:00 - 13:00</option>
                                        <option value="13:00-14:00">13:00 - 14:00</option>
                                        <option value="14:00-15:00">14:00 - 15:00</option>
                                        <option value="15:00-16:00">15:00 - 16:00</option>
                                        <option value="16:00-17:00">16:00 - 17:00</option>                                   
                                    </select>
                                </div>
								
                            </div>
							<div class="row">
                                <div class="col-md-12">
								<label>Opis problema</label>
                                    <textarea name="problem_description" value="" placeholder="Opis problema" required></textarea>
                                </div>
                            </div> 
							<div class="row">
                                <div class="col-md-12 text-center">
							<div class="alert">
							<?php include('errors.php'); ?>
							</div>
							</div>
							</div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" name="app_reg"  class="btn theme-btn"  >Pošalji prijavu</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Appointment Area -->
        
        
         <!-- Start Footer -->
         <?php include ('footer.php'); ?>
        <!-- End Footer -->
        
		<!-- Bootstrap JS file -->
		<script src="assets/js/bootstrap.min.js"></script>
		<!-- Datepicker JS file -->
		<script src="assets/js/datepicker.js"></script>
		<!-- WOW JS file -->
		<script src="assets/js/wow.min.js"></script>
        <!-- Custom JS file -->
        <script src="assets/js/main.js"></script>
		
	</body>

</html>