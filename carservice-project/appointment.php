<?php

require("include/db.php");
//preventing sql injections
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
  }
// initializing variables
$plate_number = "";
$select_services    = "";
$date    = "";
$time    = "";
$problem_description = "";
$errors = array();
 $_SESSION['success'] = "";


// connect to the database
$db = mysqli_connect('mysql687.loopia.se', 'autoserv@b44759', '5Rs)3kjS4!~s#', 'bojanmaricic_com');

// REGISTER USER
if (isset($_POST['app_reg'])) {
  // receive all input values from the form
  $plate_number = test_input($_POST['plate_number']);
  $plate_number = mysqli_real_escape_string($db, $plate_number);
  $select_services = test_input($_POST['select_services']);  
  $select_services = mysqli_real_escape_string($db, $select_services);
  $date = test_input($_POST['date']);
  $date = mysqli_real_escape_string($db, $date);
  $time = test_input($_POST['time']);
  $time = mysqli_real_escape_string($db, $time);
  $problem_description = test_input($_POST['problem_description']);
  $problem_description = mysqli_real_escape_string($db, $problem_description);

  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  if (empty($plate_number)) { array_push($errors, "Unesi registraciju");
  } else {
    if (!preg_match("/^[a-zA-Z0-9]*$/",$plate_number)) {
      array_push($errors, "Za registarski broj su dozvoljeni samo slova i brojevi!"); 
    }
      if (strlen($plate_number)!=7) {
        array_push($errors, "Registarski broj mora da ima 7 karaktera!");
      }
  }

  if (empty($select_services)) { array_push($errors, "Izaberi uslugu"); }
  if (empty($date)) { array_push($errors, "Izaberi datum"); }
  if (empty($time)) { array_push($errors, "Izaberi termin"); }
  if (empty($problem_description)) { array_push($errors, "Unesi opis problema"); }
  

  // first check the database to make sure 
  // a user does not already exist with the same username and/or email
  $user_check_query = "SELECT * FROM problems WHERE date='$date' OR time='$time' LIMIT 1";
  $result = mysqli_query($db, $user_check_query);
  $user = mysqli_fetch_assoc($result);
  


  // Finally, register user if there are no errors in the form
  if (count($errors) == 0) {

  	$query = "INSERT INTO problems (plate_number, select_services, date, time, problem_description) 
  			  VALUES('$plate_number', '$select_services', '$date', '$time', '$problem_description')";
  	mysqli_query($db, $query);
  	$_SESSION['success'] = "Uspesno ste ulogovani";
  header('location: index.php');
  }
}
?>