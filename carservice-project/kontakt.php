<!DOCTYPE html>
<html lang="en">
	
<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Automehaničarska radionica | Kontakt</title>
        <!-- Favicon -->
        <link rel="icon" href="assets/img/favicon-icon.png">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<!-- Font-awesome CSS -->
		<link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
		<!-- Flaticon CSS -->
		<link rel="stylesheet" href="assets/fonts/flaticon.css">
		<!-- Datepicker CSS -->
		<link rel="stylesheet" href="assets/css/datepicker.css">
		<!-- Animate CSS -->
		<link rel="stylesheet" href="assets/css/animate.css">
		<!-- Style CSS -->
		<link rel="stylesheet" href="assets/css/style.css">
		<!-- Responsive CSS -->
		<link rel="stylesheet" href="assets/css/responsive.css">			
		<!-- jQuery min js -->
		<script src="assets/js/jquery-1.12.4.min.js"></script>
		
			<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Kalam|Roboto+Condensed:300,400,700" rel="stylesheet">

	</head>
	<body>
     
        
        <!-- Start Header -->
         <?php include ('header.php'); ?>
        <!-- End Header -->
        
       
        
        <!-- Start Breadcumbs Area -->
        <div class="breadcumbs-area breadcumbs-banner">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h2>Kontakt</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcumbs Area -->
        
        
        <!-- Contact Form Area -->
        <div class="content-block-area contact-us">
            <div class="container">
               <h3 class="area-title">Kontakt podaci</h3>
                
                
                <div class="contact-form-area">
                    <div class="row">
                        <div class="col-sm-5 col-md-4">
                              
								<div class="media">
									<div class="media-left">
										<i class="fa fa-home"></i>
									</div>
									<div class="media-body">
										<span>Vojvođanska 54, Sombor</span>
									</div>
								</div>
							
									<div class="media">
										<div class="media-left">
											<i class="fa fa-phone"></i>
										</div>
										<div class="media-body">
											<span>025 444 555</span>
										</div>
									</div>
								
									<div class="media">
										<div class="media-left">
											<i class="fa fa-mobile"></i>
										</div>
										<div class="media-body">
											<span>063 11 55 55</span>
										</div>
									</div>
                    
									<div class="media">
										<div class="media-left">
											<i class="fa fa-envelope"></i>
										</div>
										<div class="media-body">
											<span>info@autoservis.com</span>
										</div>
									</div>
                  
						</div>
                        
						<div class="col-sm-7 col-md-8">
                            <form method="POST" action="contact/contact_send.php" id="myform" name="contactForm">
                                <div class="col-sm-6"> 
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control"  placeholder="Ime" >
                                    </div>
                                </div>

                                <div class="col-sm-6"> 
                                    <div class="form-group">
                                        <input type="email" name="email" class="form-control"  placeholder="Email" >
                                    </div>
                                </div>

                                <div class="col-sm-12"> 
                                    <div class="form-group">
                                        <textarea class="form-control" id="field" onkeyup="countChar(this)" name="comments" placeholder="Tekst poruke" cols="30" rows="8" ></textarea>
										<div class="brojac" id="brojac" name="textarea"></div>
									</div>
                                    <div id="contact_send_status"></div>
                                    <button type="submit" class="btn theme-btn">Pošalji poruku</button>
                                </div>
                            </form>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        <!-- End Contact Form Area -->
        
        <!-- Google Map Area -->
        <div id="map"></div>
        <!-- Google Map Area -->    
            
        <!-- Start Footer -->
         <?php include ('footer.php'); ?>
        <!-- End Footer -->
<script>
    $("#myform").validate({
        rules: {
            name: {
                required: true,
                minlength: 3,
                maxlength: 15
            },
            email: "required email",
            },
			
        messages: {
            name: {
                required: "<span> Unesite Vaše ime!</span>",
                minlength: jQuery.validator.format("<span>Ime mora sa ima najmanje {0} karaktera!</span>"),
                maxlength: jQuery.validator.format("<span>Ime može da ima najviše {0} karaktera!</span>")
            },
            email: {
                required: "<span> Unesite Vašu email adresu!</span>",
                email: "<span> Email mora biti u ovom formatu : name@domain.com</span>"
            },
        }
    });

    function countChar(character) {
    var len = character.value.length;
        if (len >= 250) 
        {
            character.value = character.value.substring(0, 250);
        } else 
        {
            $('#brojac').text( 250 - len + ' karaktera je preostalo' );
        }
    };
</script>	
		<!-- Bootstrap JS file -->
		<script src="assets/js/bootstrap.min.js"></script>
		<!-- Datepicker JS file -->
		<script src="assets/js/datepicker.js"></script>
		<!-- WOW JS file -->
		<script src="assets/js/wow.min.js"></script>		
		<!-- Google maps -->
        <script type="text/javascript" src='http://maps.google.com/maps/api/js?key=AIzaSyCMxLp8-TRNwFIyAV2hmkXgpRsdYtgrVsU'></script>      
        <script src="assets/js/gmaps.min.js"></script>   
        <!-- Custom JS file -->
        <script src="assets/js/main.js"></script>
	
	</body>
</html>