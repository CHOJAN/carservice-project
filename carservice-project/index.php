
<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Automehaničarska radionica</title>
        <!-- Favicon -->
        <link rel="icon" href="assets/img/favicon-icon.png">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<!-- Font-awesome CSS -->
		<link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
		<!-- Flaticon CSS -->
		<link rel="stylesheet" href="assets/fonts/flaticon.css">
		<!-- Datepicker CSS -->
		<link rel="stylesheet" href="assets/css/datepicker.css">
		<!-- Animate CSS -->
		<link rel="stylesheet" href="assets/css/animate.css">
		<!-- Style CSS -->
		<link rel="stylesheet" href="assets/css/style.css">
		<!-- Responsive CSS -->
		<link rel="stylesheet" href="assets/css/responsive.css">
		<!-- jQuery min js -->
		<script src="assets/js/jquery-1.12.4.min.js"></script>
	</head>
	
	<body>
        
        <!-- Start Header -->
         <?php include ('header.php'); ?>
        <!-- End Header -->
  
        <!-- Start Homepage Slider -->
            <div class="homepage-slider slider-image">
                <div class="d-table">
                    <div class="d-table-cell">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h1>Preko 20 godina iskustva u struci</h1>
                                    <p>Zakazite servis vašeg vozila. Da bi ste se prijavili neophodno je da se registrujete i prijavite popunjavanjem registracione prijave.</p>
                                    <?php  if(isset($_SESSION['id_user']))
											{
												echo		"<a class=\"btn theme-btn\" href=\"zakazi-servis.php\">Zakaži servis</a>";
											}
											else 
											{
												echo		"<a class=\"btn theme-btn\" href=\"prijava.php\">Prijava/Registracija</a>"; 	
											}
									?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- End Homepage Slider -->
        
        
        <!-- Start Services Area -->
        <div class="content-block-area bg-gray">
            <div class="container">
                <div class="row">
                   <div class="col-md-6 col-md-offset-3">
                       <div class="section-title text-center">
                           <h2>Naše<span> usluge </span></h2>
                           <div class="icon-cars"><img src="assets/img/cars.png" alt="car"></div>
                           <p>Možemo da vam ponudimo širok asortiman usluga za vaše vozilo u našoj automehaničarskoj radionici.</p>
                       </div>
                   </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="featured-boxed text-center wow fadeInLeft">
                           <div class="octagonWrap">
                                <div class="octagon"><i class="flaticon-car-with-wrench"></i></div>
                            </div>
                            <h3>Popravka vozila</h3>
                            <div class="upper-line"></div>
                            <div class="bottom-line"></div>
                            <p>Usluge dijagnostike i servisa vaših automobila vršimo kvalitetno, brzo, povoljno i koristimo isključivo originalne auto delove.</p>
                        </div>
                    </div>
                    
                    <div class="col-sm-3">
                        <div class="featured-boxed text-center wow fadeInRight ">
                           <div class="octagonWrap">
                                <div class="octagon"><i class="flaticon-oil"></i></div>
                            </div>
                            <h3>Održavanje vozila</h3>
                            <div class="upper-line"></div>
                            <div class="bottom-line"></div>
                            <p>Postanite naša stalna mušterija. Održavamo vaše vozilo prema potrebi i specifikaciji proizvođača i vodimo urednu evidenciju za svako održavanje. </p>
                        </div>
                    </div>
                    
                    <div class="col-sm-3">
                        <div class="featured-boxed text-center wow fadeInLeft">
                           <div class="octagonWrap">
                                <div class="octagon"><i class="flaticon-engine"></i></div>
                            </div>
                            <h3>Prodaja auto delova</h3>
                            <div class="upper-line"></div>
                            <div class="bottom-line"></div>
                            <p>Mi smo ovlašćeni uvoznik i distributer auto delova renomiranih proizvođača. Potrebne rezerve auto delove možete kupiti u našoj prodavnici. </p>
                        </div>
                    </div>
					<div class="col-sm-3">
                        <div class="featured-boxed text-center wow fadeInRight">
                           <div class="octagonWrap">
                                <div class="octagon"><i class="flaticon-car-breakdown"></i></div>
                            </div>
                            <h3>Šlep šlužba</h3>
                            <div class="upper-line"></div>
                            <div class="bottom-line"></div>
                            <p>Posedujemo specijalno vozilo opremljeno za prevoz novih vozila i vozila koja su imala saobraćajnu nezgodu ili kvar. </p>
                        </div>
                    </div>
                </div>
            </div>
   
        </div>
        <!-- End Services Area -->
    
        <!-- Start Why Choose Us Area -->
        <div class="content-block-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                       <div class="section-title text-center">
                           <h2>Zašto nas <span>izabrati</span></h2>
                           <div class="icon-cars"><img src="assets/img/cars.png" alt="car"></div>
                           <p>Više je razloga zašto izabrati našu automehaničarsku radionicu. Mi izdvajamo neke od naših prednosti koje nas razlikuju od drugih.</p>
                       </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="boxed-item">
                            <span class="single-boxed"><i class="flaticon-domain"></i></span>
                            <h3>Online zakazivanje</h3>
                            <p>Zakažite online servis vašeg vozila i u dogovorenom terminu dolazimo da ga preuzimemo.</p>  
                        </div>
                    </div>
                    
                    <div class="col-sm-6 col-md-3">
                        <div class="boxed-item">
                            <span class="single-boxed"><i class="flaticon-wall-clock"></i></span>
                            <h3>Efikasnost</h3>
                            <p>Popravka ili servis vašeg vozila biće izvršena kvalitetno u najkraćem mogućem roku. </p>  
                        </div>
                    </div>
                    
                    <div class="col-sm-6 col-md-3">
                        <div class="boxed-item">
                            <span class="single-boxed"><i class="flaticon-mechanic-tools"></i></span>
                            <h3>Iskustvo i znanje</h3>
                            <p>Za servis i popravku vašeg vozila zaduženi su stručni i obučeni majstori koji imaju dugogodišnje iskustvo.</p>  
                        </div>
                    </div>
                    
                    <div class="col-sm-6 col-md-3">
                        <div class="boxed-item">
                            <span class="single-boxed"><i class="flaticon-dollar-coin-stack"></i></span>
                            <h3>Povoljne cene</h3>
                            <p>Cene naših usluga i originalni auto delovi nude vam se po povoljnim cenama. </p>  
                        </div>
                    </div>
                </div>
                
                <div class="col-md-12 text-center">
                    <a href="/o-nama" class="btn theme-btn">više o nama</a>
                </div>
            </div>
        </div>
        <!-- End Why Choose Us Area -->
       	
		<!-- Start Our Parners Area -->
        <div class="content-block-area bg-gray">
            <div class="container">
               <div class="row">
                   <div class="col-md-6 col-md-offset-3">
                       <div class="section-title text-center">
                           <h2><span>Naši</span> partneri</h2>
                           <div class="icon-cars"><img src="assets/img/cars.png" alt="car"></div>
                           <p>Mi smo ovlašćeni zastupnici i serviseri različitih proizvođača automobila za Republiku Srbiju. Ugrađujemo i prodajemo originalne auto delove za više tipova vozila.</p>

					  </div>
                   </div>
                </div>
                       
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">                                                                 
										<div class="col-md-2">
											<div class="partners-logo">
												<img src="assets/img/alfa.png" alt="Image">
											</div>
										</div>
										<div class="col-md-2">
											<div class="partners-logo">
												<img src="assets/img/audi.png" alt="Image">
											</div>
										</div>
										<div class="col-md-2">
											<div class="partners-logo">
												<img src="assets/img/bmw.png" alt="Image">
											</div>
										</div>
										<div class="col-md-2">
											<div class="partners-logo">
												<img src="assets/img/mazda.png" alt="Image">
											</div>
										</div>
										<div class="col-md-2">
											<div class="partners-logo">
												<img src="assets/img/mercedes.png" alt="Image">
											</div>
										</div>
										<div class="col-md-2">
											<div class="partners-logo">
												<img src="assets/img/toyota.png" alt="Image">
											</div>
										</div>
								  </div>
                                </div>
                            </div>
            </div>
        </div>
        <!-- End Our Parners Area -->
            
         <!-- Start Footer -->
         <?php include ('footer.php'); ?>
        <!-- End Footer -->
        
		
		<!-- Bootstrap JS file -->
		<script src="assets/js/bootstrap.min.js"></script>
		<!-- Datepicker JS file -->
		<script src="assets/js/datepicker.js"></script>
		<!-- WOW JS file -->
		<script src="assets/js/wow.min.js"></script>
        <!-- Custom JS file -->
        <script src="assets/js/main.js"></script>
	</body>

</html>